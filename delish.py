from urllib.request import Request, urlopen
from bs4 import BeautifulSoup as soup
from multiprocessing import Process
import json
import string
import csv
import pandas as pd
import re

# def get_urls(page_soup,url_list):
# 	try:
# 		container = page_soup.find_all("li", class_ = 'm-PromoList__a-ListItem')
# 		for li in container:
# 			url = li.find('a')
# 			url_list.append('https:'+url['href'])
# 		return url_list
# 	except:
# 		return ''


def get_image(page_soup):
	try:
		try:
			container = page_soup.find('div',class_ = 'embed embed-image embed-image-center embed-image-medium')
			image = container.find('img')['data-src']

		except:
			container = page_soup.find('div', class_='content-lede-image recipe-lede-image')
			image = container.find('img')['data-src']
		return image
	except:
		return ''


def get_title(page_soup):
	try:
		title = page_soup.find("h1", class_="content-hed recipe-hed").text
		return title.strip()
	except:
		return ''


def get_introduction(page_soup):
	try:
		introduction = page_soup.find("div",class_ = ['recipe-introduction show-less','recipe-introduction show-more'])
		return introduction.text.strip()
	except:
		return ''


def get_ingredients(page_soup):
	try:
		ingredients_list = []
		ingredients_container = page_soup.find_all("div",class_="ingredient-item")
		for item in ingredients_container:
			ingredients_list.append(re.sub(r'\t', '', item.text.strip()))
		return ','.join(ingredients_list)
	except:
		return ''


def get_directions(page_soup):
	try:
		directions_list = []
		directions_container = page_soup.find("div", class_="directions-body")
		all_directions = directions_container.find_all('li')
		count = 1
		for item in all_directions:
			directions_list.append('STEP:{}'.format(count) + '-' + item.text.strip())
			count = count + 1

		return directions_list
	except:
		return ''


def get_yield(page_soup):
	try:
		container = page_soup.find("span", class_='yields-amount')
		# yield_ = container.find('span', class_ = "o-RecipeInfo__a-Description").text
		return re.sub(r'\t', '', container.text.strip())
	except:
		return ''


def get_cooking_time(page_soup):
	try:
		cook_time = ''
		container = page_soup.find("ul", class_ = 'o-RecipeInfo__m-Time')
		title = container.find_all("span",class_= 'o-RecipeInfo__a-Headline')[1].text
		if title.strip() == 'Cook:':
			cook_time = container.find_all("span",class_= 'o-RecipeInfo__a-Description')[1].text
		return cook_time
	except:
		return ''


def get_preparation_time(page_soup):
	try:
		prep_time = page_soup.find("span", class_='prep-time-amount').text

		return ' '.join(re.sub(r'\n', '',  prep_time.strip()).split())
	except:
		return ''


def get_total_time(page_soup):
	try:
		total_time = page_soup.find("span", class_='total-time-amount').text
		return ' '.join(re.sub(r'\n', '', total_time.strip()).split())
	except:
		return ''


def get_category(page_soup):
	try:
		container = page_soup.find("div", class_='o-Capsule__m-TagList m-TagList')
		category_list = []
		category = container.find_all("a")
		for item in category:
			category_list.append(item.text.strip())
		return ','.join(category_list)
	except:
		return ''


def get_nutrition(page_soup):
	try:
		nutrition_list = []
		container = page_soup.find_all('dt',class_='m-NutritionTable__a-Headline')
		for item in container:
			next_element = item.find_next_sibling("dd")
			title_text  = str(item).split()[1].split('class="m-NutritionTable__a-Headline">')[1].split('</dt>')[0]
			description  = str(next_element).split()[1].split('class="m-NutritionTable__a-Description">')[1].split('</dd>')[0]
			nutrition_list.append("{}: {}".format(title_text,description))
		return ','.join(nutrition_list)


	except:
		return ''





def com_scrapper(url_list,num,total_range):
	com_list = []
	fields = ['image', 'ingredients', 'directions', 'cooking_time', 'preparation_time', 'yield', 'level']

	print('scrapping started count no:{} total range{}'.format(num, total_range))

	for url in url_list:
		try:
			req = Request(url, headers={'User-Agent': 'Mozilla/5.0'})

			webpage = urlopen(req).read()
			page_soup = soup(webpage, "html.parser")
			title = get_title(page_soup)
			image = get_image(page_soup)
			ingredients = get_ingredients(page_soup)
			directions = get_directions(page_soup)
			total_time = get_total_time(page_soup)
			preparation_time = get_preparation_time(page_soup)
			yield_ = get_yield(page_soup)
			introduction = get_introduction(page_soup)
			category = get_category(page_soup)
			nutrition_info = get_nutrition(page_soup)
			recipe_dict = {
				'title':title,
				'image':image,
				'introduction': introduction,
				'ingredients':ingredients,
				'directions':directions,
				'preparation_time':preparation_time,
				'total_time':total_time,
				'yield':yield_
			}
			com_list.append(recipe_dict)

		except:
			pass

	df = pd.DataFrame(com_list)
	df.to_csv('delish_recipies.csv', mode='a', header=False, index=False)


if __name__ == '__main__':

	with open('delish-com_urls.json') as fp:
		url_list = json.load(fp)
	total_range =0


	while total_range<=500:
		count = total_range
		count_list = [url_list[count:count+100], url_list[count+100:count+200], url_list[count+200:count+300],url_list[count+300:count+400],url_list[count+400:count+500]]
		Pros = []
		for i in range(len(count_list)):
			p = Process(target=com_scrapper, args=(count_list[i],i,total_range))
			Pros.append(p)
			p.start()

			# block until all the threads finish (i.e. block until all function_x calls finish)
		for t in Pros:
			t.join()
		total_range = total_range+500