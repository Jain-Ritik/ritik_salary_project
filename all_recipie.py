from urllib.request import Request, urlopen
from bs4 import BeautifulSoup as soup
from multiprocessing import Process
import json
import string
import csv
import pandas as pd


def get_image(page_soup):
	try:
		container = page_soup.find("div", class_=["image-container","hero-photo__wrap"])
		# image_container = container.find("div",class_="inner-container js-inner-container  image-overlay")
		image = container.find("img")
		return image['src']
	except:
		return ''


def get_level(page_soup):
	try:
		container = page_soup.find("ul",class_="o-RecipeInfo__m-Level")
		level = container.find_all("span",class_= 'o-RecipeInfo__a-Description')[0].text
		return level
	except:
		return ''


def get_title(page_soup):
	try:
		title = page_soup.find("h1", class_=["recipe-summary__h1","headline heading-content"]).text
		return title.strip()
	except:
		return ''


def get_ingredients(page_soup):
	try:
		ingredients_list = []
		ingredients_container = page_soup.find_all("li",class_=["ingredients-item","checkList__line"])
		for item in ingredients_container:
			ingredients_list.append(item.text.strip())
		return ','.join(ingredients_list)

	except:
		return ''


def get_directions(page_soup):
	try:
		directions_list = []
		directions_container = page_soup.find_all("li", class_=["subcontainer instructions-section-item","step"])
		count = 1
		for item in directions_container:
			try:
				paragraph = item.find('p')
				directions_list.append("Step:{}-{}".format(count,paragraph.text.strip()))
			except:
				directions_list.append("Step:{}-{}".format(count,item.text.strip()))
			count = count+1

		return directions_list
	except:
		return ''


def get_yield(page_soup):
	try:
		try:
			container = page_soup.find_all("div", class_='two-subcol-content-wrapper')[1]
			yield_ = container.find_all("div", class_='recipe-meta-item-body')[1].text
		except:
			yield_ = page_soup.find("meta", attrs={"itemprop":'recipeYield'})['content'] + ' ' + 'servings'



		return yield_.strip()
	except:
		return ''


def get_cooking_time(page_soup):
	try:
		try:
			container = page_soup.find_all("div", class_='two-subcol-content-wrapper')[0]
			cooking_time = container.find_all("div", class_='recipe-meta-item-body')[1].text
		except:
			cooking_time = page_soup.find_all("span", class_='prepTime__item--time')[1].text


		return cooking_time.strip()
	except:
		return ''


def get_preparation_time(page_soup):
	try:
		try:
			container = page_soup.find_all("div", class_ = 'two-subcol-content-wrapper')[0]
			prep_time = container.find_all("div",class_= 'recipe-meta-item-body')[0].text
		except:
			prep_time = page_soup.find_all("span", class_='prepTime__item--time')[0].text

		return prep_time.strip()
	except:
		return ''


def get_total_time(page_soup):
	try:
		total_time = ''
		try:
			total_time = page_soup.find_all("span", class_='prepTime__item--time')[2].text
		except:
			container = page_soup.find_all("div", class_ = 'two-subcol-content-wrapper')[0]
			total_time_title = container.find_all("div", class_='recipe-meta-item-header')[2].text
			if total_time_title.strip()  == "total:":
				total_time = container.find_all("div", class_='recipe-meta-item-body')[2].text


		return total_time.strip()
	except:
		return ''


def get_category(page_soup):
	try:
		category_list = []
		categories = page_soup.find_all("meta", {'itemprop':'recipeCategory'})

		for item in categories:
			category_list.append(item['content'])
		return ','.join(category_list)
	except:
		return ''


def get_nutrition(page_soup):
	try:
		try:
			nutrition_info = page_soup.find('div',class_='nutrition-summary-facts')
		except:
			container = page_soup.find('section',class_ = 'nutrition-section container')
			nutrition_info = container.find('div', class_='section-body')


		return nutrition_info.text.strip()
	except:
		return ''


def com_scrapper(url_list,num,total_range):
	com_list = []
	fields = ['image', 'ingredients', 'directions', 'cooking_time', 'preparation_time', 'yield', 'level']

	print('scrapping started count no:{} total range{}'.format(num, total_range))

	for url in url_list:
		try:
			req = Request(url, headers={'User-Agent': 'Mozilla/5.0'})

			webpage = urlopen(req).read()
			page_soup = soup(webpage, "html.parser")
			image = get_image(page_soup)
			ingredients = get_ingredients(page_soup)
			directions = get_directions(page_soup)
			cooking_time = get_cooking_time(page_soup)
			preparation_time = get_preparation_time(page_soup)
			total_time = get_total_time(page_soup)
			title = get_title(page_soup)
			yield_ = get_yield(page_soup)
			level = get_level(page_soup)
			nutrition = get_nutrition(page_soup),
			categories = get_category(page_soup)
			recipe_dict = {
				'title':title,
				'image':image,
				'ingredients':ingredients,
				'directions':directions,
				'cooking_time':cooking_time,
				'preparation_time':preparation_time,
				'total_time':total_time,
				'yield':yield_,
				'nutrition_info':nutrition,
				'categories':categories
			}
			com_list.append(recipe_dict)

		except:
			pass

	df = pd.DataFrame(com_list)
	df.to_csv('all_recipies.csv', mode='a',header=False, index=False)


if __name__ == '__main__':

	with open('all_recipies_urls.json') as fp:
		url_list = json.load(fp)
	total_range = 0


	while total_range<=1000:
		count = total_range
		count_list = [url_list[count:count+100], url_list[count+100:count+200], url_list[count+200:count+300],url_list[count+300:count+400],url_list[count+400:count+500]]
		Pros = []
		for i in range(len(count_list)):
			p = Process(target=com_scrapper, args=(count_list[i],i,total_range))
			Pros.append(p)
			p.start()

			# block until all the threads finish (i.e. block until all function_x calls finish)
		for t in Pros:
			t.join()
		total_range = total_range+500