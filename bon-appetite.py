from urllib.request import Request, urlopen
from bs4 import BeautifulSoup as soup
from multiprocessing import Process
import json
import string
import csv
import pandas as pd
import re
from recipe_scrapers import scrape_me


def get_introduction(page_soup):
	try:
		introduction = page_soup.find("h2",class_ = 'dek--basically')
		return introduction.text.strip()
	except:
		return ''


def get_directions(page_soup):
	try:
		directions_list = []
		directions_container = page_soup.find_all("div", class_="steps-wrapper")
		# all_directions = directions_container.find_all('li')
		count = 1
		for item in directions_container:
			steps = item.find_all('li',class_ = 'step')
			for step in steps:

				directions_list.append('STEP:{}'.format(count) + '-' + step.text.strip())
				count = count + 1

		return directions_list
	except:
		return ''

def get_preparation_time(page_soup):
	try:
		prep_time = page_soup.find("span", class_='prep-time-amount').text

		return ' '.join(re.sub(r'\n', '',  prep_time.strip()).split())
	except:
		return ''


def get_nutrition(page_soup):
	try:

		nutrition_info = page_soup.find('h3',class_='subhed recipe__nutrition__header').find_next_sibling('span')
		return nutrition_info.text.strip()


	except:
		return ''





def com_scrapper(url_list,num,total_range):
	com_list = []
	fields = ['image', 'ingredients', 'directions', 'cooking_time', 'preparation_time', 'yield', 'level']

	print('scrapping started count no:{} total range{}'.format(num, total_range))
	for url in url_list:
		try:
			req = Request(url, headers={'User-Agent': 'Mozilla/5.0'})

			webpage = urlopen(req).read()
			page_soup = soup(webpage, "html.parser")
			scraper = scrape_me(url)
			title = scraper.title()
			image = scraper.image()
			ingredients = scraper.ingredients()
			directions = get_directions(page_soup)
			yield_ = scraper.yields()
			introduction = get_introduction(page_soup)
			nutrition_info = get_nutrition(page_soup)
			recipe_dict = {
				'title':title,
				'image':image,
				'introduction': introduction,
				'ingredients':ingredients,
				'directions':directions,
				'nutrition_info':nutrition_info,
				'yield':yield_
			}
			com_list.append(recipe_dict)


		except:
			pass

	df = pd.DataFrame(com_list)
	df.to_csv('bon-appetite_recipies.csv', mode='a', header=False, index=False)

if __name__ == '__main__':

	with open('bon-appetite_urls.json') as fp:
		url_list = json.load(fp)
	total_range =0


	while total_range<=500:
		count = total_range
		count_list = [url_list[count:count+100], url_list[count+100:count+200], url_list[count+200:count+300],url_list[count+300:count+400],url_list[count+400:count+500]]
		Pros = []
		for i in range(len(count_list)):
			p = Process(target=com_scrapper, args=(count_list[i],i,total_range))
			Pros.append(p)
			p.start()

			# block until all the threads finish (i.e. block until all function_x calls finish)
		for t in Pros:
			t.join()
		total_range = total_range+500