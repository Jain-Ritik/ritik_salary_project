from urllib.request import Request, urlopen
from bs4 import BeautifulSoup as soup
from multiprocessing import Process
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
chrome_options = Options()
chrome_options.add_argument("--headless")
# # chrome_options.add_argument("--incognito")
chrome_options.add_argument("--start-maximized")

import json
import string
import csv
import pandas as pd
from selenium import webdriver
import time
# def get_urls(page_soup,url_list):
# 	try:
# 		container = page_soup.find_all("li", class_ = 'm-PromoList__a-ListItem')
# 		for li in container:
# 			url = li.find('a')
# 			url_list.append('https:'+url['href'])
# 		return url_list
# 	except:
# 		return ''


def get_image(page_soup):
	try:
		image = page_soup.find('img',class_ ='recipe-image__img' )

		return image['src']

		# container = post_container.find_all("div",class_='post__text')
		# for post in container:
		# 	review_list.append(post.text.strip())
		# print(review_list)
		# # image = container.find("img",class_ = 'recipe-image__img')
		# return review_list
	except:
		return ''


def get_title(page_soup):
	try:
		title = page_soup.find("div",class_='recipe-title').text
		return title.strip()
	except:
		return ''


def get_ingredients(page_soup):
	try:
		ingredients_list = []
		ingredients_container = page_soup.find_all("div",class_="recipe-ingredients__ingredient")
		for item in ingredients_container:
			ingredients_list.append(" ".join(item.text.split()))
		return ','.join(ingredients_list)
	except:
		return ''


def get_directions(page_soup):
	try:
		directions_list = []
		directions_container = page_soup.find_all("li", class_="recipe-directions__step")
		count = 1
		for item in directions_container:
			directions_list.append('STEP:{}'.format(count)+'-'+item.text.strip())
			count = count+1

		return directions_list
	except:
		return ''


def get_yield(page_soup):
	try:
		container = page_soup.find("div", class_='recipe-facts__details recipe-facts__yield')
		yield_ = container.find('a', class_ = "theme-color").text
		return yield_.strip()
	except:
		return ''


def get_cooking_time(page_soup):
	try:
		cook_time = page_soup.find("div",class_= 'recipe-facts__details recipe-facts__time').text
		return cook_time.strip()
	except:
		return ''


def get_category(page_soup):
	try:
		container = page_soup.find("div", class_ = 'recipe-breadcrumbs')
		category = container.find("span",class_= 'recipe-breadcrumbs__text category').text
		return category.strip()
	except:
		return ''


def get_nutrition(page_soup):
	try:
		nutrition_list = []
		nutrition = page_soup.find('section',class_='recipe-nutrition__info')
		nutrition_info = nutrition.find_all('p',class_ = 'recipe-nutrition__item')
		for item in nutrition_info:
			nutrition_list.append(item.text.strip())

		# for item in container:
		# 	next_element = item.find_next_sibling("dd")
		# 	title_text  = str(item).split()[1].split('class="m-NutritionTable__a-Headline">')[1].split('</dt>')[0]
		# 	description  = str(next_element).split()[1].split('class="m-NutritionTable__a-Description">')[1].split('</dd>')[0]
		# 	nutrition_list.append("{}: {}".format(title_text,description))
		return nutrition_list



	except:
		return ''



def com_scrapper(url_list,num,total_range):

	com_list = []
	fields = ['image', 'ingredients', 'directions', 'cooking_time', 'preparation_time', 'yield', 'level']

	print('scrapping started count no:{} total range{}'.format(num, total_range))
	driver = webdriver.Chrome(chrome_options=chrome_options)

	for url in url_list:
		try:
			# req = Request(url, headers={'User-Agent': 'Mozilla/5.0'})
			# webpage = urlopen(req).read()
			driver.get(url)
			try:
				WebDriverWait(driver, 7). \
					until(EC.visibility_of_element_located((By.XPATH, ".//img[@class='recipe-image__img']")))
			except:
				pass
			page_soup = soup(driver.page_source, "html.parser")
			ingredients = get_ingredients(page_soup)
			image = get_image(page_soup)
			directions = get_directions(page_soup)
			cooking_time = get_cooking_time(page_soup)
			category = get_category(page_soup)
			yield_ = get_yield(page_soup)
			title = get_title(page_soup)
			nutrition_info = get_nutrition(page_soup)
			recipe_dict = {
				'title': title,
				'ingredients':ingredients,
				'directions':directions,
				'total_time':cooking_time,
				'category':category,
				'yield':yield_,
				'nutrition':nutrition_info,
				'image':image

			}
			print(recipe_dict)
			com_list.append(recipe_dict)
		except:
			pass

	df = pd.DataFrame(com_list)
	df.to_csv('food-com_recipies.csv', mode='a', header=False, index=False)
	driver.quit()


if __name__ == '__main__':

	with open('food-com_url.json') as fp:
		url_list = json.load(fp)
	total_range = 0


	while total_range<=10000:
		count = total_range
		count_list = [url_list[count:count+100], url_list[count+100:count+200], url_list[count+200:count+300],url_list[count+300:count+400],url_list[count+400:count+500]]
		Pros = []
		for i in range(len(count_list)):
			p = Process(target=com_scrapper, args=(count_list[i],i,total_range))
			Pros.append(p)
			p.start()

			# block until all the threads finish (i.e. block until all function_x calls finish)
		for t in Pros:
			t.join()
		total_range = total_range+500